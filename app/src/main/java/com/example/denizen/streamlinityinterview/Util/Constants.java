package com.example.denizen.streamlinityinterview.Util;

/**
 * Created by denizen on 12/7/17.
 */

public interface Constants {
    String BASE_URL = "https://davids-restaurant.herokuapp.com/";

    String API_KEY = "api_key";

    String CATEGORY_TYPE_BUNDLE = "categoryTypeBundle";
}
