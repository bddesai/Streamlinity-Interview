package com.example.denizen.streamlinityinterview.Service;

import android.database.Observable;

import com.example.denizen.streamlinityinterview.Model.Category;
import com.example.denizen.streamlinityinterview.Model.MenuResponse;
import com.example.denizen.streamlinityinterview.Model.ModelResponse;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by Bhavin Desai on 12/2/17.
 */

public interface ApiInterface {
    @GET("categories.json")
    Call<List<Category>> getCategories();

    @GET("menu_items.json")
    Call<MenuResponse> getMenuItemsList(@Query("category") String category);
}
