package com.example.denizen.streamlinityinterview.Controller;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Toast;

import com.example.denizen.streamlinityinterview.Model.MenuItem;
import com.example.denizen.streamlinityinterview.Model.MenuResponse;
import com.example.denizen.streamlinityinterview.R;
import com.example.denizen.streamlinityinterview.Service.ApiClient;
import com.example.denizen.streamlinityinterview.Service.ApiInterface;
import com.example.denizen.streamlinityinterview.Util.Constants;
import com.example.denizen.streamlinityinterview.View.MenuItemAdapter;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MenuActivity extends AppCompatActivity {

    private static String TAG = MenuActivity.class.getSimpleName();
    private String categoryType;

    private ApiInterface apiService;

    private RecyclerView recyclerViewMenu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);


        categoryType = getIntent().getStringExtra(Constants.CATEGORY_TYPE_BUNDLE);
        //Toast.makeText(this, "Category Type : "+categoryType, Toast.LENGTH_SHORT).show();

        recyclerViewMenu = findViewById(R.id.recyclerViewMenu);
        recyclerViewMenu.setLayoutManager(new LinearLayoutManager(this));

        fetchMenuList();
    }


    public void fetchMenuList(){
        // Get API Client
        apiService = ApiClient.getClient().create(ApiInterface.class);

        // Make an API Call
        Call<MenuResponse> call = apiService.getMenuItemsList(categoryType);

        // fetch and handle responses
        call.enqueue(new Callback<MenuResponse>() {
            @Override
            public void onResponse(Call<MenuResponse> call, Response<MenuResponse> response) {
                List<MenuItem> menuItemList = response.body().getMenuItemArrayList();
                // populate Menu Items
                recyclerViewMenu.setAdapter(new MenuItemAdapter(menuItemList, MenuActivity.this));
            }

            @Override
            public void onFailure(Call<MenuResponse> call, Throwable t) {
                // Log error here since request failed
                Log.e(TAG, t.toString());
                Toast.makeText(MenuActivity.this,
                        getString(R.string.service_error_message), Toast.LENGTH_SHORT).show();
            }
        });
    }
}
