package com.example.denizen.streamlinityinterview.View;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.denizen.streamlinityinterview.Controller.MainActivity;
import com.example.denizen.streamlinityinterview.Model.Category;
import com.example.denizen.streamlinityinterview.R;

import java.util.List;

/**
 * Created by denizen on 12/9/17.
 */

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.CategoryViewHolder> {

    List<Category> categoryList;
    private Context context;

    public CategoryAdapter(List<Category> categoryList,  Context context) {
        this.categoryList = categoryList;
        this.context = context;
    }

    @Override
    public CategoryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.category_list_item, parent, false);
        return new CategoryViewHolder(view);
    }

    @Override
    public void onBindViewHolder(CategoryViewHolder holder, int position) {
        String categoryName = categoryList.get(position).getName();
        String categoryType = categoryList.get(position).getShortName();

        holder.categoryName.setText(categoryName+"  -  ");
        holder.categoryType.setText(categoryType);
        holder.itemView.setOnClickListener(view -> ((MainActivity)context).onClickCategoryCalled(categoryType));
    }

    @Override
    public int getItemCount() {
        return categoryList.size();
    }

    public static class CategoryViewHolder extends RecyclerView.ViewHolder {
        CardView categoryLayout;
        TextView categoryName;
        TextView categoryType;

        public CategoryViewHolder(View itemView) {
            super(itemView);
            categoryLayout = itemView.findViewById(R.id.categoryLayout);
            categoryName  = itemView.findViewById(R.id.categoryNameText);
            categoryType = itemView.findViewById(R.id.categoryTypeText);
        }
    }
}
