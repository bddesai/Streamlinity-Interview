package com.example.denizen.streamlinityinterview.Model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Bhavin Desai on 12/2/17.
 */

public class ModelResponse {

    @SerializedName("parenttag")
    private ArrayList<Model> modelArrayList;

    public ArrayList<Model> getModelArrayList() {
        return modelArrayList;
    }

    public void setPassArrayList(ArrayList<Model> passArrayList) {
        this.modelArrayList = passArrayList;
    }
}
