package com.example.denizen.streamlinityinterview.Model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by denizen on 12/9/17.
 */

public class MenuResponse {
    @SerializedName("menu_items")
    private ArrayList<MenuItem> menuItemArrayList;

    public ArrayList<MenuItem> getMenuItemArrayList() {
        return menuItemArrayList;
    }

    public void setMenuItemArrayList(ArrayList<MenuItem> menuItemArrayList) {
        this.menuItemArrayList = menuItemArrayList;
    }
}
