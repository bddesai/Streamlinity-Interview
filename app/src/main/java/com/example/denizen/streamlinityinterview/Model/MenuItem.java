package com.example.denizen.streamlinityinterview.Model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by denizen on 12/9/17.
 */

public class MenuItem {

    @SerializedName("id")
    int id;
    @SerializedName("short_name")
    String short_name;
    @SerializedName("name")
    String name;
    @SerializedName("description")
    String description;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getShort_name() {
        return short_name;
    }

    public void setShort_name(String short_name) {
        this.short_name = short_name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
