package com.example.denizen.streamlinityinterview.Model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Bhavin Desai on 12/2/17.
 */

public class Model {

    @SerializedName("id")
    int id;

    @SerializedName("name")
    long name;

    public Model(int id, long name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public long getName() {
        return name;
    }

    public void setName(long name) {
        this.name = name;
    }
}
