package com.example.denizen.streamlinityinterview.View;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.denizen.streamlinityinterview.Model.MenuItem;
import com.example.denizen.streamlinityinterview.R;

import java.util.List;

/**
 * Created by denizen on 12/9/17.
 */

public class MenuItemAdapter extends RecyclerView.Adapter<MenuItemAdapter.MenuItemViewHolder> {

    List<MenuItem> menuList;
    private Context context;

    public MenuItemAdapter(List<MenuItem> menuList, Context context) {
        this.menuList = menuList;
        this.context = context;
    }

    @Override
    public MenuItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.menu_list_item, parent, false);
        return new MenuItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MenuItemViewHolder holder, int position) {
        String menuItemName = menuList.get(position).getName();
        holder.menuName.setText(menuItemName);
    }

    @Override
    public int getItemCount() {
        return  menuList.size();
    }

    public static class MenuItemViewHolder extends RecyclerView.ViewHolder {
        CardView menuLayout;
        TextView menuName;

        public MenuItemViewHolder(View itemView) {
            super(itemView);
            menuLayout = itemView.findViewById(R.id.menuItemLayout);
            menuName = itemView.findViewById(R.id.menuNameText);
        }
    }
}