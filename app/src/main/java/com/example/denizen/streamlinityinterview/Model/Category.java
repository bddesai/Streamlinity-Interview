package com.example.denizen.streamlinityinterview.Model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by denizen on 12/9/17.
 */

public class Category {

    @SerializedName("id")
    int id;
    @SerializedName("short_name")
    String short_name;
    @SerializedName("name")
    String name;
    @SerializedName("special_instructions")
    String special_instructions;
    @SerializedName("url")
    String url;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getShortName() {
        return short_name;
    }

    public void setShortName(String shortName) {
        this.short_name = shortName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSpecial_instructions() {
        return special_instructions;
    }

    public void setSpecial_instructions(String special_instructions) {
        this.special_instructions = special_instructions;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
