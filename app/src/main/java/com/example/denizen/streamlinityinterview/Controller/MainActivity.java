package com.example.denizen.streamlinityinterview.Controller;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Toast;

import com.example.denizen.streamlinityinterview.Model.Category;
import com.example.denizen.streamlinityinterview.R;
import com.example.denizen.streamlinityinterview.Service.ApiClient;
import com.example.denizen.streamlinityinterview.Service.ApiInterface;
import com.example.denizen.streamlinityinterview.Util.Constants;
import com.example.denizen.streamlinityinterview.View.CategoryAdapter;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    private static String TAG = MainActivity.class.getSimpleName();

   //private Button button;
    private ApiInterface apiService;

    private RecyclerView recyclerViewCategories;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerViewCategories = findViewById(R.id.recyclerViewCategories);
        recyclerViewCategories.setLayoutManager(new LinearLayoutManager(this));

        fetchCategories();
    }

    private void fetchCategories(){

        // Get API Client
        apiService = ApiClient.getClient().create(ApiInterface.class);

        // Make an API Call
        Call<List<Category>> call = apiService.getCategories();

        // fetch and handle responses
        call.enqueue(new Callback<List<Category>>() {
            @Override
            public void onResponse(Call<List<Category>> call, Response<List<Category>> response) {
                List<Category> categories = response.body();
                // populate passes
                recyclerViewCategories.setAdapter(new CategoryAdapter(categories, MainActivity.this));
            }

            @Override
            public void onFailure(Call<List<Category>> call, Throwable t) {
                // Log error here since request failed
                Log.e(TAG, t.toString());
                Toast.makeText(MainActivity.this,
                        getString(R.string.service_error_message), Toast.LENGTH_SHORT).show();
            }
        });
    }


    public void onClickCategoryCalled(String categoryType){
        Intent intent = new Intent(this, MenuActivity.class);
        intent.putExtra(Constants.CATEGORY_TYPE_BUNDLE, categoryType);
        startActivity(intent);
    }

}
